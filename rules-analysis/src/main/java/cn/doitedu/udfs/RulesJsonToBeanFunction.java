package cn.doitedu.udfs;


import cn.doitedu.pojo.RulesBean;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;

public class RulesJsonToBeanFunction extends ProcessFunction<String, RulesBean> {


    @Override
    public void processElement(String value, Context ctx, Collector<RulesBean> out) throws Exception {

        try {
            JSONObject jsonObject = JSON.parseObject(value);
            RulesBean bean = jsonObject.getObject("after", RulesBean.class);
            out.collect(bean);
        } catch (Exception e) {
            //e.printStackTrace();
        }

    }

}
