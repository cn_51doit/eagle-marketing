package cn.doitedu.pojo;


import cn.doitedu.service.QueryService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FactBean {

    private LogBean logBean;

    private RuleCondition ruleCondition;

    private QueryService queryService;

    private boolean match;
}
