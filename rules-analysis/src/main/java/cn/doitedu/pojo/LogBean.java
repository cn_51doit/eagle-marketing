package cn.doitedu.pojo;

import lombok.Data;

import java.util.Map;

//自动生成getter、setter方法
@Data
public class LogBean {
    private String account        ;
    private String appId          ;
    private String appVersion     ;
    private String carrier        ;
    private String deviceId       ;
    private String deviceType     ;
    private String ip             ;
    private double latitude       ;
    private double longitude      ;
    private String netType        ;
    private String osName         ;
    private String osVersion      ;
    private String releaseChannel ;
    private String resolution     ;
    private String sessionId      ;
    private long timeStamp        ;
    private String eventId        ;
    private Map<String,String> properties;

    public EventStateBean toEventStateBean() {
        return new EventStateBean(timeStamp, eventId, properties);
    }

    private boolean match;

}