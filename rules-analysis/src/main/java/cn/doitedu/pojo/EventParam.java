package cn.doitedu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventParam {

    //行为
    private String eventId;

    //行为属性
    private Map<String, String> properties;

    //起始时间
    private long startTime;

    //结束时间
    private long endTime;

    //阈值
    private int threshold;

    //SQL
    private String sql;

    public EventParam(String eventId, Map<String, String> properties) {
        this.eventId = eventId;
        this.properties = properties;
    }
}
