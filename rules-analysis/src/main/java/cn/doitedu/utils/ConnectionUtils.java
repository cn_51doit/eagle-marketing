package cn.doitedu.utils;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import redis.clients.jedis.Jedis;

import java.sql.DriverManager;

/**
 * ConnectionUtils在哪一端被初始化的？ 在TaskManager中别初始化和调用的
 */
public class ConnectionUtils {

    public static Connection getHbaseConn(ParameterTool parameterTool) throws Exception{
        org.apache.hadoop.conf.Configuration hbaseConf = HBaseConfiguration.create();
        hbaseConf.set("hbase.zookeeper.quorum", parameterTool.getRequired("hbase.zookeeper.quorum"));
        hbaseConf.setInt("hbase.zookeeper.property.clientPort", parameterTool.getInt("hbase.zookeeper.property.clientPort", 2181));
        return ConnectionFactory.createConnection(hbaseConf);
    }

    public static java.sql.Connection getClickHouseConn(ParameterTool parameterTool) throws Exception{
        return DriverManager.getConnection(parameterTool.getRequired("clickhouse.url"));
    }


    public static Jedis getJedis(ParameterTool parameterTool) {
        String host = parameterTool.getRequired("redis.host");
        int port = parameterTool.getInt("redis.port", 6379);
        String password = parameterTool.getRequired("redis.password");
        int db = parameterTool.getInt("redis.db", 0);
        Jedis jedis = new Jedis(host, port);
        jedis.auth(password);
        jedis.select(db);
        return jedis;
    }

    public static java.sql.Connection getDorisDbConn(ParameterTool parameterTool) throws Exception{

        String url = parameterTool.getRequired("dorisdb.url");
        String username = parameterTool.getRequired("dorisdb.user");
        String password = parameterTool.getRequired("dorisdb.pwd");
        return DriverManager.getConnection(url, username, password);

    }
}
