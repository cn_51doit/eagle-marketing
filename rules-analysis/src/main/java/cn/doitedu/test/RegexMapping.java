package cn.doitedu.test;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 测试正则的映射
 */
public class RegexMapping {

    public static void main(String[] args) {

        //用户实际的行为数据
        List<String> events = Arrays.asList("product_view", "product_addCart", "product_view",
                "product_pay", "product_addCart", "product_force", "product_view", "product_addCart", "product_reject");

        //事先定义的规则（期望的事件）
        //将product_view 映射成 1
        //product_force  映射成 2
        //product_addCart 映射出 3
        //将实际的事件字符串映射成短的字符串或数字，可以提高正则表达式的效率，方便正则表达式的书写
        List<String> targetEvents = Arrays.asList("product_view", "product_force", "product_addCart");

        StringBuffer sb = new StringBuffer();

        //规则映射（将长的字符串映射成数字或字母）
        //循环实际的数据
        for (String event : events) {
            int index = targetEvents.indexOf(event);
            if (index >= 0) {
                sb.append((char)(index + 65));
            } else {
                sb.append(index + 1);
            }


        }


        System.out.println(sb.toString());

//        String regStr = "(.*?1.*?2.*?3)";
//        Pattern pattern = Pattern.compile(regStr);
//        Matcher matcher = pattern.matcher(sb.toString());
//        int count = 0;
//        while (matcher.find()) {
//            count++;
//        }

        //System.out.println(count);

    }


}
