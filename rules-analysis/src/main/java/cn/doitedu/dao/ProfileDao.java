package cn.doitedu.dao;

import cn.doitedu.pojo.LogBean;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.hadoop.hbase.client.Connection;

import java.util.Map;

/**
 * DAO : Data Access Object （数据访问对象，即用户查询外部数据库的类）
 *
 * ProfileDao ：接口，接口中定义方法规范（方法的名称、方法的参数、方法的返回值，没有具体实现）
 * 以后可以定义一个到多个实现，想要用那个实现就是要那个实现，也可以不改变代码灵活的切换实现类
 */
public interface ProfileDao {

    boolean isMatchProfile(LogBean bean, Map<String, String> profileCondition) throws Exception;

    void close() throws Exception;

    void init(ParameterTool parameterTool) throws Exception;
}
